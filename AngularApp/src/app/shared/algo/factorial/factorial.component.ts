import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-factorial',
  templateUrl: './factorial.component.html',
  styleUrls: ['./factorial.component.css']
})
export class FactorialComponent implements OnInit {

  result: number = 0;
  elapsed: string = '';
  input: number;

  constructor() { }

  ngOnInit() {
  }

  Calculate() {
    console.log(`Starting Factorial calculation for ${this.input}`);
    var start = performance.now();
    
    this.result = this.getFactorial(this.input);
    var end = performance.now();
    console.log(`Done in ${end-start} ms`);
    this.elapsed = (end-start).toFixed(2).toString();
  }

  getFactorial(x: number): number {
    var result = 1;
    while (x != 1)
    {
        result = result * x;
        x = x - 1;
    }
    return result;
  }

}
