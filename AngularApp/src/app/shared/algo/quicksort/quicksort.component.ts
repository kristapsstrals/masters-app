import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-quicksort',
  templateUrl: './quicksort.component.html',
  styleUrls: ['./quicksort.component.css']
})
export class QuicksortComponent implements OnInit {

  result: string = '';
  elapsed: string = '';
  input: number;

  constructor() { }

  ngOnInit() {
  }

  Calculate() {
    console.log(`Starting Quicksort for array of size ${this.input}`);
    var arr = this.generateArray(this.input);
    var start = performance.now();
    this.quickSort(arr, 0, arr.length - 1);
    var end = performance.now();
    console.log(`Done in ${end-start} ms`);
    this.elapsed = (end-start).toFixed(2).toString();
    this.result = 'Done';
  }

  quickSort(arr: number[], left: number, right: number): number[] {

    var index;

    if (arr.length > 1) {

        index = this.partition(arr, left, right);

        if (left < index - 1) {
            this.quickSort(arr, left, index - 1);
        }

        if (index < right) {
            this.quickSort(arr, index, right);
        }

    }

    return arr;
  }

  generateArray(x: number): number[] {
    return Array.from({length: x}, () => Math.floor(Math.random() * 1000));
  }

  partition(arr: number[], left: number, right: number): number{
    var pivot   = arr[Math.floor((right + left) / 2)],
        i       = left,
        j       = right;


    while (i <= j) {

        while (arr[i] < pivot) {
            i++;
        }

        while (arr[j] > pivot) {
            j--;
        }

        if (i <= j) {
            this.swap(arr, i, j);
            i++;
            j--;
        }
    }

    return i;
  }

  swap(arr: number[], left: number, right: number) {
    var temp = arr[left];
    arr[left] = arr[right];
    arr[right] = temp;
  }

}
