import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArrayReverseComponent } from './array-reverse.component';

describe('ArrayReverseComponent', () => {
  let component: ArrayReverseComponent;
  let fixture: ComponentFixture<ArrayReverseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArrayReverseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArrayReverseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
