import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-array-reverse',
  templateUrl: './array-reverse.component.html',
  styleUrls: ['./array-reverse.component.css']
})
export class ArrayReverseComponent implements OnInit {

  result: string = '';
  elapsed: string = '';
  input: number;

  constructor() { }

  ngOnInit() {
  }

  Calculate() {
    var start = performance.now();
    console.log(`Generating random array with size: ${this.input}`);

    var arr: number[] = this.generateArray(this.input);
    console.log('Array generated, Reversing...');

    this.reverseArray(arr);

    var end = performance.now();
    console.log(`Done in ${end-start} ms`);
    this.elapsed = (end-start).toFixed(2).toString();

    this.result = 'Done';
  }

  generateArray(x: number): number[] {
    return Array.from({length: x}, () => Math.floor(Math.random() * 1000));
  }

  reverseArray(arr: number[]): void {
    var i = 0;
    var j = arr.length - 1;
    while (i < j)
    {
        var temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
        i++;
        j--;
    }
  }

}
