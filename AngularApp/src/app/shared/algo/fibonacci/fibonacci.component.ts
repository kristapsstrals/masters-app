import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fibonacci',
  templateUrl: './fibonacci.component.html',
  styleUrls: ['./fibonacci.component.css']
})
export class FibonacciComponent implements OnInit {

  result: number = 0;
  elapsed: string = '';
  input: number;

  constructor() { }

  ngOnInit() {
  }

  Calculate() {
    console.log(`Starting Fibonacci calculation for ${this.input}`);
    var start = performance.now();
    // this.result = this.fibonacci(this.input);
    this.result = this.fibonacciIterative(this.input);
    var end = performance.now();
    console.log(`Done in ${end-start} ms`);
    this.elapsed = (end-start).toFixed(2).toString();
  }

  fibonacci(n: number): number{
    if (n == 1) return 1;
    if (n == 2) return 1;
    // console.log('Current number: ' + n);
    return this.fibonacci(n-1) + this.fibonacci(n-2);
  }

  fibonacciIterative(n: number): number {
    if (n === 0) {
      return 0;
    }
    if (n === 1) {
      return 1;
    }
    
    var previous = 0;
    var current = 1;
    
    for (var i=2; i <=n; i++) {
      var temp = previous + current;
      previous = current;
      current = temp;
    }
    
    return current;
  }

}
