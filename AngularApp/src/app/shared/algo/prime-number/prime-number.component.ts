import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-prime-number',
  templateUrl: './prime-number.component.html',
  styleUrls: ['./prime-number.component.css']
})
export class PrimeNumberComponent implements OnInit {

  result: string = '';
  elapsed: string = '';
  input: number;

  constructor() { }

  ngOnInit() {
  }

  Calculate() {
    console.log(`Starting Fibonacci calculation for ${this.input}`);
    var start = performance.now();
    // this.result = this.fibonacci(this.input);
    this.result = this.isPrimeNumber(this.input) ? 'Jā' : 'Nē';
    var end = performance.now();
    console.log(`Done in ${end-start} ms`);
    this.elapsed = (end-start).toFixed(2).toString();
  }

  isPrimeNumber(x: number): boolean{
    if (x == 1) return false;
    if (x == 2) return true;

    for (var i = 2; i <= Math.ceil(Math.sqrt(x)); ++i)  {
    if (x % i == 0)  return false;
    }

    return true;
  }

}
