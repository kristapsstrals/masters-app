import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  todos: Todo[] = [];
  input: string = '';
  todoCount: number;

  constructor() { }

  ngOnInit() {
    this.todos.push({Title: 'piemērs', IsDone: false});
    this.todoCount = this.todos.filter(t => t.IsDone).length;
  }

  AddTodo(){
    var todo: Todo = {Title: this.input, IsDone: false};
    this.todos.push(todo);
    this.input = '';
  }

}

interface Todo{
  Title: string;
  IsDone: boolean;
}