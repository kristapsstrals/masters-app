import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-fetchdata',
  templateUrl: './fetchdata.component.html',
  styleUrls: ['./fetchdata.component.css']
})
export class FetchdataComponent implements OnInit {

  forecasts: WeatherForecast[];
  weatherUrl: string = 'assets/sample-data/weather.json';
  constructor(private http: HttpClient) { }

  ngOnInit() {
    // this.getWeather()
    // // .subscribe(data => this.forecasts = {...data});
    // .subscribe(data => console.log(data));

    this.getWeather().subscribe(data => this.forecasts = data);
    console.log(this.forecasts);
  }

  getWeather() {
    return this.http.get<WeatherForecast[]>(this.weatherUrl);
  }

}

interface WeatherForecast
{
  Date: Date; 
  TemperatureC: number; 
  TemperatureF: number; 
  Summary: string; 
}