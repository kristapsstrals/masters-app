import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent implements OnInit {
  
  currentCount: number = 0;
  constructor() { }


  IncrementCount(): void{
      this.currentCount++;
  }

  ngOnInit() {
  }

}
