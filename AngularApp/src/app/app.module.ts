import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { NavmenuComponent } from './shared/navmenu/navmenu.component';
import { HomeComponent } from './pages/home/home.component';
import { ExamplesComponent } from './pages/examples/examples.component';
import { FetchdataComponent } from './pages/fetchdata/fetchdata.component';
import { CounterComponent } from './pages/counter/counter.component';
import { TodoComponent } from './pages/todo/todo.component';
import { FibonacciComponent } from './shared/algo/fibonacci/fibonacci.component';
import { ArrayReverseComponent } from './shared/algo/array-reverse/array-reverse.component';
import { FactorialComponent } from './shared/algo/factorial/factorial.component';
import { PrimeNumberComponent } from './shared/algo/prime-number/prime-number.component';
import { QuicksortComponent } from './shared/algo/quicksort/quicksort.component';


const appRoutes: Routes = [
  {
    path: 'examples',
    component: ExamplesComponent,
    data: { title: 'Examples' }
  },
  {
    path: 'fetchdata',
    component: FetchdataComponent,
    data: { title: 'Fetch data' }
  },
  {
    path: 'todo',
    component: TodoComponent,
    data: { title: 'Todo binding' }
  },
  {
    path: 'home',
    component: HomeComponent,
    data: { title: 'Angular app' }
  },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
];


@NgModule({
  declarations: [
    AppComponent,
    NavmenuComponent,
    HomeComponent,
    ExamplesComponent,
    FetchdataComponent,
    CounterComponent,
    TodoComponent,
    FibonacciComponent,
    ArrayReverseComponent,
    FactorialComponent,
    PrimeNumberComponent,
    QuicksortComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      // { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
