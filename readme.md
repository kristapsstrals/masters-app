# Final try at the blazor-angular project for Master Degree

TODO:

- Make script to build each one, and copy dist to servers
- for developer, just develop each using its own server (`ng serve` and `dotnet watch run`)

To get both of the apps running under one node server:

- Change the base URL for both apps to `/blazor/` and `/angular/` respectively
- change all blazor links in `NavMenu.cshtml` to include `/blazor/*link*` - this should not be needed in further *Blazor* versions
- run `dotnet publish -o dist` in `BlazorApp` directory and copy the innermost `dist` folder to `server/blazor` directory
- run `ng build --prod` in `AngularApp` directory and copy the `dist` folder to `server/angular` directory
- run `node app.js` in `server` directory and enjoy.

This can also be published to *Heroku*, but there seems to be an issue currently when loading up the *Blazor* app as the *dll* files are there in Heroku console and return code *200*, but when loading the website, they return code *503*.