namespace BlazorApp.Shared.ArrayReverse
{
    using System;
    using System.Linq;
    using BlazorApp.Shared.Benchmark;
    public static class ArrayReverseCode
    {
        public static TimeSpan ReverseRandomArray(int size)
        {
            Console.WriteLine($"Generating random array with size: {size}");
            var arr = BenchmarkHelper.GetRandomArray(size);
            Console.WriteLine("Array generated, Reversing...");
            var start = DateTime.Now;
            int i = 0;
            int j = arr.Length - 1;
            while (i < j)
            {
                var temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j--;
            }

            return DateTime.Now - start;
        }
    }
}