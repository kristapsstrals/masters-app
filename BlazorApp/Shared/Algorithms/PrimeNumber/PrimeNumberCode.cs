namespace BlazorApp.Shared.PrimeNumber
{
    using System;

    public static class PrimeNumberCode
    {
        public static bool IsPrime(long number)
        {
            if (number == 1) return false;
            if (number == 2) return true;

            for (long i = 2; i <= Math.Ceiling(Math.Sqrt(number)); ++i)  {
            if (number % i == 0)  return false;
            }

            return true;
        }
    }
}