namespace BlazorApp.Shared.Factorial
{
    using System;
    using System.Numerics;

    public static class FactorialCode
    {
        public static string GetFactorial(long number)
        {
            var result = CalculateFactorial(number);
            return result.ToString("E");
        }

        static BigInteger CalculateFactorial(long number)
        {

            //Recursion in browser exceeds call stack
            //if (number == 1)
            //    return 1;
            //else
            //    return number * CalculateFactorial(number - 1);

            BigInteger result = BigInteger.One;
            while (number != 1)
            {
                //Console.WriteLine("Number: " + number);
                result = result * number;
                number = number - 1;
            }
            return result;
        }
    }
}