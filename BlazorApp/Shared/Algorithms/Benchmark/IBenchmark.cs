namespace BlazorApp.Shared.Benchmark
{
    public interface IBenchmark
    {
        string Title { get; } 
        string Description { get; }

        string InputText { get; }
    }
}