namespace BlazorApp.Shared.Benchmark
{
    using System.Threading.Tasks;
    using System;
    using System.Numerics;
    public class Fibonacci : IBenchmark
    {
        //Fibonacci numbers
        public long GetNth(long n) {
            if (n == 1) return 1;
            if (n == 2) return 1;
            return GetNth(n-1) + GetNth(n-2);
        }

        public long GetNthIterative(long n)
        {
            // f(0) = 0
            // f(1) = 1
            // f(n) = f(n-1) + f(n-2)
            if (n == 0) 
            {
                return 0;
            }
            if (n == 1) 
            {
                return 1;
            }
            
            var previous = 0L;
            var current = 1L;
            
            for (var i=2L; i <= n; i++) {
                var temp = previous + current;
                previous = current;
                current = temp;
            }
            
            return current;
        }

        public string GetNthBig(long n)
        {

            if (n == 0) 
            {
                return "0";
            }
            if (n == 1) 
            {
                return "1";
            }

            var big = BigInteger.Parse(n.ToString());
            
            var previous = BigInteger.Zero;
            var current = BigInteger.One;
            
            for (var i=2L; i <= n; i++) {
                var temp = previous + current;
                previous = current;
                current = temp;
                // Console.WriteLine($"Current number: {current.ToString()}");
            }
            
            return current.ToString("E");
        }

        public decimal GetNthDecimal(long n)
        {
            if (n == 0) 
            {
                return 0;
            }
            if (n == 1) 
            {
                return 1;
            }
            
            var previous = 0M;
            var current = 1M;
            
            for (var i=2L; i <= n; i++) {
                var temp = previous + current;
                previous = current;
                current = temp;
                // Console.WriteLine($"Current number: {current}");
            }
            
            return current;
        }

        public string Title => "Fibonacci skaitlis";

        public string Description => "Fibonocci n-tā skaitļa aprēķins izmantojot rekursīvo metodi";

        public string InputText => "Kārtas skaitlis" ;
    }
}