namespace BlazorApp.Shared.Benchmark
{
    public enum BenchmarkType
    {
        Fibonacci,
        QuickSort
    }
}