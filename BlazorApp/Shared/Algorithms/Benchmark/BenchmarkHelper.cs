namespace BlazorApp.Shared.Benchmark
{
    using System;
    using System.Linq;
    public static class BenchmarkHelper
    {
        public static int[] GetRandomArray(int size)
        {
            int Min = 0;
            int Max = 1000;
            Random randNum = new Random();
            int[] arr = Enumerable
                .Repeat(0, size)
                .Select(i => randNum.Next(Min, Max))
                .ToArray();

            return arr;
        }
    }
}