//Register necessary JS methods here

//Example:
// Blazor.registerFunction('showRawHtml', (elementId, html) => {
//     var el = document.getElementById(elementId);
//     el.innerHTML = html;
//     return true;
// });

//Use in C# like this:
// var articleBody = CommonMark.CommonMarkConverter.Convert(article.body);
// RegisteredFunction.Invoke<bool>("showRawHtml", "article-body", articleBody);

Blazor.registerFunction('showAlert', (text) => {
    alert(text);
    return true;
});