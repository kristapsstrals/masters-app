const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const app = express();

// Blazor location
const blazor = require('./blazor/blazor-api');
app.use('/blazor', blazor);

// Angular location
const angular = require('./angular/angular-api');
app.use('/angular', angular);

// app.get('/', (req, res) => {
//     res.json({title: 'This is the project. to go to blazor app: /blazor; go to angular: /angular'});
// });

app.get('/',function(req,res){
    res.sendFile(path.join(__dirname + '/index.html'));
    //__dirname : It will resolve to your project folder.
});

//Set Port
const port = process.env.PORT || '3000';
app.set('port', port);

const server = http.createServer(app);

server.listen(port, () => console.log(`Running on localhost:${port}`));