# Create a combined app on one node server

Setup:

- Clone the repository
- launch the app using `node app.js` or `npm run start`

## Routes

All `Blazor` routes are under `/blazor`. Same goes for `/angular`.   
When adding links be sure to prefix all links with the required `baseurl` else they won't work. (In Blazor)

e.g. `<a href="/blazor/examples">Examples</a>`